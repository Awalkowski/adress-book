# Address Book project
Made with **Python 3.10** and **Django 4.0**

Simple CRUD application that acts like a address book.

### Main features:
- List all address records
- Create new record
- Detail view for given record
- Update existing record
- Delete record
- Search records
- Log all SQL through Django logger
- Log in and log out features
- All key features are protected. User needs to log in before performing any of the action listed above.

## URLS:
- /address_book/
- /address_book/create
- /address_book/update_record/<id>
- /address_book/delete_record/<id>
- /address_book/login
- /address_book/logout

## Installation

**Simple download adn run:**
Make sure docker and docker compose are installed

1. git clone project
2. navigate to the main app directory
3. run script build_n_run.sh

For Windows users there might be need for using winpty to run commands from build_n_run.sh
- before each command put "winpty" keyword, eg. winpty docker-compose up --build -d

### When application is up and running:
Navigate to 127.0.0.1:8000/address_book/. This should redirect you to the login page.
Log in using these credentials:
- username : tester
- password : password

You should see screen like this:
![img.png](img.png)

## For developers
Read this if you are software developer and want to run this project on your machine without using docker:
1. git clone the project
2. create postgres database and change project django settings accordingly to use your database
3. ideally create virtual env and install requirements from requirements.txt file
4. make sure to have environment settings set with SECRET_KEY

### If you are going to commit any new changes don't forget to:
- pre-commit install

### When application is up and running:
- Run django command: python manage.py create_test_user

Navigate to 127.0.0.1:8000/address_book/. This should redirect you to the login page.
Log in using these credentials:
- username : tester
- password : password

You should see screen like this:
![img.png](img.png)


## Running tests
To run tests simply execute following command:
### Docker version:
- docker-compose exec web python ./manage.py test --keepdb
### Local version:
- python manage.py test --keepdb
