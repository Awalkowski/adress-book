import logging

from django.contrib.auth.models import User
from django.core.management import BaseCommand

logger = logging.getLogger(__name__)


class Command(
    BaseCommand,
):
    help = "Create test users"

    def handle(self, *args, **options):
        create_superuser()
        logger.info("Test user successfully created!")


def create_superuser():
    username = "tester"
    password = "password"
    email = "test@email.com"
    superuser = User.objects.create_superuser(username=username, email=email, password=password)
    logger.info(f"Created superuser: {superuser}")
