from django import forms
from django.utils.translation import gettext

from addressbook.models import BookRecord


class AddressBookForm(forms.ModelForm):
    id = forms.IntegerField(required=False, widget=forms.HiddenInput())
    first_name = forms.CharField(required=False, max_length=30)
    last_name = forms.CharField(required=False, max_length=50)
    name = forms.CharField(required=False, max_length=50)
    image = forms.ImageField(required=False)
    city = forms.CharField(required=True, max_length=50)
    post_code = forms.CharField(required=True, max_length=6)
    building_number = forms.CharField(required=True, max_length=5)
    local_number = forms.CharField(required=False, max_length=5)

    def clean(self):
        super().clean()
        first_name = self.cleaned_data.get("first_name")
        last_name = self.cleaned_data.get("last_name")
        name = self.cleaned_data.get("name")
        if not (first_name or last_name or name):
            raise forms.ValidationError({"name": gettext("at least one name field is required")})

    def clean_name(self):
        name = self.cleaned_data.get("name")
        if name and BookRecord.objects.filter(name=name).exists():
            raise forms.ValidationError(gettext("That name is already taken"))

    class Meta:
        model = BookRecord
        fields = "__all__"
