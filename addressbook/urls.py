from django.urls import path

from addressbook.views import (
    LoginView,
    LogoutView,
    RecordCreateView,
    RecordDeleteView,
    RecordDetailView,
    RecordsIndexView,
    RecordUpdateView,
)

app_name = "addressbook"
urlpatterns = [
    path("", RecordsIndexView.as_view(), name="index"),
    path("record/<int:pk>", RecordDetailView.as_view(), name="detail"),
    path("create", RecordCreateView.as_view(), name="create"),
    path("update_record/<int:pk>", RecordUpdateView.as_view(), name="update"),
    path("delete_record/<int:pk>", RecordDeleteView.as_view(), name="delete"),
    path("login", LoginView.as_view(), name="login"),
    path("logout", LogoutView.as_view(), name="logout"),
]
