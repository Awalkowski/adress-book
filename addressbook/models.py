from django.core.validators import RegexValidator
from django.db import models
from django.utils.translation import gettext_lazy as _
from phonenumber_field.modelfields import PhoneNumberField


class TimestampModel(models.Model):
    created_at = models.DateTimeField(_("created at"), auto_now_add=True)
    updated_at = models.DateTimeField(_("modified at"), auto_now=True)

    class Meta:
        abstract = True


class BookRecord(TimestampModel):
    first_name = models.CharField(verbose_name="first name", max_length=30, null=True, blank=True)
    last_name = models.CharField(verbose_name="last name", max_length=50, null=True, blank=True)
    name = models.CharField(verbose_name="name", max_length=50, null=True, blank=True)
    image = models.ImageField(upload_to="images/", blank=True, null=True)

    city = models.CharField(verbose_name=_("city"), max_length=50, null=False, blank=False)
    post_code = models.CharField(
        verbose_name=_("post code"),
        max_length=6,
        null=False,
        blank=False,
    )
    building_number = models.CharField(verbose_name=_("building number"), max_length=5, null=False, blank=False)
    local_number = models.CharField(verbose_name=_("local number"), max_length=5, null=False, blank=False)

    class Meta:
        verbose_name = _("book record")
        verbose_name_plural = _("book records")

    def __str__(self):
        return f"{self.first_name} {self.last_name}" if self.first_name or self.last_name else f"{self.name}"


class Phone(TimestampModel):
    phone = PhoneNumberField(verbose_name="phone", null=False, blank=False)
    address_record = models.ForeignKey(
        BookRecord,
        verbose_name=_("address record"),
        on_delete=models.CASCADE,
        related_name="phones",
        null=True,
    )

    class Meta:
        verbose_name = _("phone")
        verbose_name_plural = _("phones")

    def __str__(self):
        return f"{self.phone}"


class Fax(TimestampModel):
    fax = models.CharField(
        verbose_name="fax",
        max_length=12,
        null=False,
        blank=False,
        validators=[
            RegexValidator(
                regex=r"^\+?[0-9]{6,}$",
                message=_("Invalid fax number, eg. +123456"),
            ),
        ],
    )
    address_record = models.ForeignKey(
        BookRecord,
        verbose_name=_("address record"),
        on_delete=models.CASCADE,
        related_name="faxes",
        null=True,
    )

    class Meta:
        verbose_name = _("fax")
        verbose_name_plural = _("fax numbers")

    def __str__(self):
        return f"{self.fax}"


class Email(TimestampModel):
    email = models.EmailField(verbose_name="email", max_length=100, blank=True, null=True)
    address_record = models.ForeignKey(
        BookRecord,
        verbose_name=_("address record"),
        on_delete=models.CASCADE,
        related_name="emails",
        null=True,
    )

    class Meta:
        verbose_name = _("email")
        verbose_name_plural = _("emails")

    def __str__(self):
        return f"{self.email}"
