from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.forms import inlineformset_factory, model_to_dict
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.utils.translation import gettext
from django.views import View
from django.views.generic import CreateView, DeleteView, DetailView, ListView, TemplateView, UpdateView

from addressbook.forms import AddressBookForm
from addressbook.models import BookRecord, Email, Fax, Phone


class RecordsIndexView(LoginRequiredMixin, ListView):
    template_name = "records/index.html"
    context_object_name = "book_records"

    def get_queryset(self):
        query_params = self.request.GET.get("q", "").lower()
        filters = Q()
        if query_params:
            filters |= Q(**{"first_name__iexact": query_params})
            filters |= Q(**{"last_name__iexact": query_params})

        return BookRecord.objects.filter(filters).order_by("-created_at")


EmailFormset = inlineformset_factory(
    BookRecord,
    Email,
    fields=["email"],
    can_delete_extra=False,
)
FaxFormset = inlineformset_factory(
    BookRecord,
    Fax,
    fields=["fax"],
    can_delete_extra=False,
)
PhoneFormset = inlineformset_factory(
    BookRecord,
    Phone,
    fields=["phone"],
    can_delete_extra=False,
)


class RecordDetailView(LoginRequiredMixin, DetailView):
    model = BookRecord
    template_name = "records/detail.html"

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        context = self.get_context_data(object=self.object)
        form_object = AddressBookForm(data=model_to_dict(self.object))
        context["form_object"] = form_object
        context["image"] = self.object.image
        return self.render_to_response(context)


class RecordCreateView(LoginRequiredMixin, CreateView):
    form_class = AddressBookForm
    template_name = "records/create.html"
    success_url = "success"

    def post(self, request, *args, **kwargs):
        self.object = None
        emails = EmailFormset(request.POST)
        faxes = FaxFormset(request.POST)
        phones = PhoneFormset(request.POST)
        book_record_form = AddressBookForm(request.POST, request.FILES)
        if (emails.is_valid() and faxes.is_valid() and phones.is_valid()) and book_record_form.is_valid():
            return self.form_valid(book_record_form)
        return self.form_invalid(book_record_form)

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        if self.request.POST:
            data["emails"] = EmailFormset(self.request.POST)
            data["faxes"] = FaxFormset(self.request.POST)
            data["phones"] = PhoneFormset(self.request.POST)
        else:
            data["emails"] = EmailFormset()
            data["faxes"] = FaxFormset()
            data["phones"] = PhoneFormset()
        return data

    def form_valid(self, form):
        context = self.get_context_data()
        emails = context["emails"]
        faxes = context["faxes"]
        phones = context["phones"]

        self.object = form.save()

        self.create_objects(emails)
        self.create_objects(faxes)
        self.create_objects(phones)

        return super().form_valid(form)

    def create_objects(self, model):
        if model.is_valid():
            model.instance = self.object
            model.save()

    def get_success_url(self):
        return reverse("addressbook:index")


class RecordUpdateView(LoginRequiredMixin, UpdateView):
    model = BookRecord
    form_class = AddressBookForm
    template_name = "records/update.html"

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        emails = EmailFormset(request.POST, instance=self.object)
        faxes = FaxFormset(request.POST, instance=self.object)
        phones = PhoneFormset(request.POST, instance=self.object)
        form = self.get_form()
        if (emails.is_valid() and faxes.is_valid() and phones.is_valid()) and form.is_valid():
            return self.form_valid(form)
        return self.form_invalid(form)

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        if self.request.POST:
            data["emails"] = EmailFormset(self.request.POST, instance=self.object)
            data["faxes"] = FaxFormset(self.request.POST, instance=self.object)
            data["phones"] = PhoneFormset(self.request.POST, instance=self.object)
        else:
            data["emails"] = EmailFormset(instance=self.object)
            data["faxes"] = FaxFormset(instance=self.object)
            data["phones"] = PhoneFormset(instance=self.object)

        if data["form"].is_valid():
            data["form"] = self.form_class(self.request.FILES, instance=self.object)
        return data

    def form_valid(self, form):
        context = self.get_context_data()
        emails = context["emails"]
        faxes = context["faxes"]
        phones = context["phones"]
        self.object = form.save()

        self.update_objects(emails)
        self.update_objects(faxes)
        self.update_objects(phones)

        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse("addressbook:index")

    def update_objects(self, model):
        if model.is_valid():
            model.instance = self.object
            model.save()


class RecordDeleteView(LoginRequiredMixin, DeleteView):
    model = BookRecord
    template_name = "records/delete.html"

    def get_success_url(self):
        return reverse("addressbook:index")


class LoginView(TemplateView):
    template_name = "records/login.html"

    def post(self, request, *args, **kwargs):
        username = request.POST["username"]
        password = request.POST["password"]
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return HttpResponseRedirect(self.get_redirect_url())
        else:
            messages.error(request, gettext("There was an error, try again..."))
            return HttpResponseRedirect(self.get_redirect_url())

    @staticmethod
    def get_redirect_url():
        return reverse("addressbook:index")


class LogoutView(View):
    def dispatch(self, request, *args, **kwargs):
        logout(request)
        return HttpResponseRedirect(self.get_success_url())

    @staticmethod
    def get_success_url():
        return reverse("addressbook:index")
