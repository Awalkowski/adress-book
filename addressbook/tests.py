from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse
from rest_framework import status

from addressbook.models import BookRecord, Email, Fax, Phone


class BookRecordModelTest(TestCase):
    def setUp(self):
        self.phone = Phone.objects.create(phone="+48123321123")
        self.fax = Fax.objects.create(fax="+123456")
        self.email = Email.objects.create(email="spiderman@marvel.com")

    @staticmethod
    def get_book_record():
        return BookRecord.objects.create(
            first_name="Spider",
            last_name="Man",
            city="New York",
            building_number="12c",
            post_code="12-123",
        )

    def test_can_create(self):
        record = self.get_book_record()
        Phone.objects.create(phone="+48123321123", address_record=record)
        Fax.objects.create(fax="+123456", address_record=record)
        Email.objects.create(email="spiderman@marvel.com", address_record=record)
        self.assertEqual(BookRecord.objects.get(id=record.id), record)

    def test_can_have_multiple_emails(self):
        record = self.get_book_record()
        Email.objects.create(email="spiderman@marvel.com", address_record=record)
        Email.objects.create(email="batman@marvel.com", address_record=record)
        Email.objects.create(email="thor@marvel.com", address_record=record)

        self.assertEqual(record.emails.count(), 3)

    def test_can_have_multiple_phones(self):
        record = self.get_book_record()
        Phone.objects.create(phone="+48123321123", address_record=record)
        Phone.objects.create(phone="+48098766678", address_record=record)
        Phone.objects.create(phone="+48666999888", address_record=record)

        self.assertEqual(record.phones.count(), 3)

    def test_can_have_multiple_faxes(self):
        record = self.get_book_record()
        Fax.objects.create(fax="+123456", address_record=record)
        Fax.objects.create(fax="+123456", address_record=record)
        Fax.objects.create(fax="+123456", address_record=record)

        self.assertEqual(record.faxes.count(), 3)


class BookRecordFixtures:
    @staticmethod
    def get_book_record():
        record = BookRecord.objects.create(
            first_name="Spider",
            last_name="Man",
            city="New York",
            building_number="12c",
            post_code="12-123",
            image="mock image",
        )
        Phone.objects.create(phone="+48123321123", address_record=record)
        Fax.objects.create(fax="+123456", address_record=record)
        Email.objects.create(email="spiderman@marvel.com", address_record=record)
        return record

    @staticmethod
    def get_post_data():
        return {
            "first_name": "James",
            "last_name": "Bond",
            "city": "Warsaw",
            "post_code": "12-123",
            "building_number": "15",
            "local_number": "2c",
            "phones-TOTAL_FORMS": "1",
            "phones-INITIAL_FORMS": "0",
            "phones-0-phone": "+48654654654",
            "faxes-TOTAL_FORMS": "1",
            "faxes-INITIAL_FORMS": "0",
            "faxes-0-fax": "+123456789",
            "emails-TOTAL_FORMS": "1",
            "emails-INITIAL_FORMS": "0",
            "emails-0-email": "james.bond@bond.pl",
        }


class UserAuthMixin(TestCase):
    def setUp(self):
        super().setUp()
        user = User.objects.create(username="testuser")
        user.set_password("12345")
        user.save()
        self.client.login(username="testuser", password="12345")


class BookRecordListViewTest(UserAuthMixin, BookRecordFixtures):
    @staticmethod
    def get_url():
        return reverse("addressbook:index")

    def test_can_list_records(self):
        self.get_book_record()
        response = self.client.get(self.get_url(), follow=True)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_can_search_records(self):
        record = self.get_book_record()
        response = self.client.get(self.get_url(), {"q": record.first_name}, follow=True)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.context_data["object_list"]), 1)
        self.assertEqual(response.context_data["object_list"][0], record)

    def test_search_no_results(self):
        self.get_book_record()
        response = self.client.get(self.get_url(), {"q": "blablabal"}, follow=True)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.context_data["object_list"]), 0)


class BookRecordCreateViewTest(UserAuthMixin, BookRecordFixtures):
    @staticmethod
    def get_url():
        return reverse("addressbook:create")

    def test_can_create(self):
        data = self.get_post_data()
        response = self.client.post(self.get_url(), data=data, follow=True)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(BookRecord.objects.get(first_name=data["first_name"], last_name=data["last_name"]))

    def test_cant_create_when_logout(self):
        self.client.logout()
        data = self.get_post_data()
        response = self.client.post(self.get_url(), data=data, follow=True)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(BookRecord.objects.count(), 0)

    def test_success_create_formsets_models(self):
        data = self.get_post_data()
        response = self.client.post(self.get_url(), data=data, follow=True)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        book_record = BookRecord.objects.get(first_name=data["first_name"], last_name=data["last_name"])
        self.assertEqual(book_record.phones.first(), Phone.objects.get(phone__exact=data["phones-0-phone"]))
        self.assertEqual(book_record.faxes.first(), Fax.objects.get(fax__exact=data["faxes-0-fax"]))
        self.assertEqual(book_record.emails.first(), Email.objects.get(email__exact=data["emails-0-email"]))

    def test_success_redirect_to_index(self):
        data = self.get_post_data()
        response = self.client.post(self.get_url(), data=data, follow=True)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.redirect_chain[0][0], reverse("addressbook:index"))

    def test_not_created_wrong_email(self):
        error_message = "Enter a valid email address."
        data = self.get_post_data()
        data["emails-0-email"] = "bad@email"
        response = self.client.post(self.get_url(), data=data, follow=True)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.context_data["emails"].errors[0]["email"][0], error_message)

    def test_not_created_wrong_fax(self):
        error_message = "Invalid fax number, eg. +123456"
        data = self.get_post_data()
        data["faxes-0-fax"] = "666"
        response = self.client.post(self.get_url(), data=data, follow=True)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.context_data["faxes"].errors[0]["fax"][0], error_message)

    def test_not_created_wrong_phone(self):
        error_message = "Enter a valid phone number (e.g. +12125552368)."
        data = self.get_post_data()
        data["phones-0-phone"] = "666999wrong"
        response = self.client.post(self.get_url(), data=data, follow=True)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.context_data["phones"].errors[0]["phone"][0], error_message)


class BookRecordDetailViewTest(UserAuthMixin, BookRecordFixtures):
    @staticmethod
    def get_url(pk):
        return reverse("addressbook:detail", kwargs={"pk": pk})

    def test_can_see_detail(self):
        record = self.get_book_record()
        response = self.client.get(self.get_url(record.id), follow=True)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(record, response.context_data["object"])

    def test_cant_get_detail_when_logged_out(self):
        self.client.logout()
        record = self.get_book_record()
        response = self.client.get(self.get_url(record.id), follow=True)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.context_data.get("object"), None)

    def test_context_includes_image(self):
        record = self.get_book_record()
        response = self.client.get(self.get_url(record.id), follow=True)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(response.context_data["image"])


class BookRecordUpdateViewTest(UserAuthMixin, BookRecordFixtures):
    @staticmethod
    def get_formset_update_data(email, address_record):
        return {
            "emails-TOTAL_FORMS": "1",
            "emails-INITIAL_FORMS": "1",
            "emails-0-email": email,
            "emails-0-id": address_record.emails.first().id,
            "emails-0-address_record": str(address_record.id),
        }

    @staticmethod
    def get_url(pk):
        return reverse("addressbook:update", kwargs={"pk": pk})

    def test_can_update(self):
        new_first_name = "Will"
        new_last_name = "Smith"
        record = self.get_book_record()

        data = self.get_post_data()
        data["first_name"] = new_first_name
        data["last_name"] = new_last_name

        response = self.client.post(self.get_url(record.id), data=data, follow=True)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(BookRecord.objects.get(first_name=new_first_name, last_name=new_last_name))

    def test_no_update_when_logout(self):
        new_first_name = "Will"
        new_last_name = "Smith"
        record = self.get_book_record()

        data = self.get_post_data()
        data["first_name"] = new_first_name
        data["last_name"] = new_last_name

        self.client.logout()
        data = self.get_post_data()
        response = self.client.post(self.get_url(record.id), data=data, follow=True)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(BookRecord.objects.filter(first_name=new_first_name, last_name=new_last_name).count(), 0)

    def test_can_update_formsets(self):
        new_email = "willsmith@smith.com"

        record = self.get_book_record()

        data = {**self.get_post_data(), **self.get_formset_update_data(new_email, record)}

        response = self.client.post(self.get_url(record.id), data=data, follow=True)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(BookRecord.objects.get(emails__email__in=[new_email]))

    def test_success_redirect_to_index(self):
        record = self.get_book_record()

        data = self.get_post_data()
        response = self.client.post(self.get_url(record.id), data=data, follow=True)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.redirect_chain[0][0], reverse("addressbook:index"))

    def test_not_updated_wrong_email(self):
        record = self.get_book_record()
        bad_mail = "bad@email"
        error_message = "Enter a valid email address."
        data = self.get_formset_update_data(bad_mail, record)
        response = self.client.post(self.get_url(record.id), data=data, follow=True)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertNotEqual(BookRecord.objects.first().emails.first(), bad_mail)
        self.assertEqual(response.context_data["emails"].errors[0]["email"][0], error_message)


class BookRecordDeleteViewTest(UserAuthMixin, BookRecordFixtures):
    @staticmethod
    def get_url(record_id):
        return reverse("addressbook:delete", kwargs={"pk": record_id})

    def test_can_delete(self):
        record = self.get_book_record()
        self.assertEqual(BookRecord.objects.count(), 1)
        response = self.client.post(self.get_url(record.id), follow=True)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertFalse(BookRecord.objects.filter(id=record.id).exists())
        self.assertEqual(BookRecord.objects.count(), 0)

    def test_cant_update_when_logout(self):
        self.client.logout()
        record = self.get_book_record()
        self.assertEqual(BookRecord.objects.count(), 1)
        response = self.client.post(self.get_url(record.id), follow=True)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(BookRecord.objects.filter(id=record.id).exists())

    def test_deleted_all_data(self):
        record = self.get_book_record()
        self.assertEqual(BookRecord.objects.count(), 1)
        response = self.client.post(self.get_url(record.id), follow=True)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertEqual(Email.objects.count(), 0)
        self.assertEqual(Phone.objects.count(), 0)
        self.assertEqual(Fax.objects.count(), 0)
