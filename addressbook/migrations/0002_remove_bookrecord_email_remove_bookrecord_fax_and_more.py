# Generated by Django 4.0 on 2022-08-30 20:09

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('addressbook', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='bookrecord',
            name='email',
        ),
        migrations.RemoveField(
            model_name='bookrecord',
            name='fax',
        ),
        migrations.RemoveField(
            model_name='bookrecord',
            name='phone',
        ),
        migrations.AddField(
            model_name='email',
            name='address_record',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='emails', to='addressbook.bookrecord', verbose_name='address record'),
        ),
        migrations.AddField(
            model_name='fax',
            name='address_record',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='faxes', to='addressbook.bookrecord', verbose_name='address record'),
        ),
        migrations.AddField(
            model_name='phone',
            name='address_record',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='phones', to='addressbook.bookrecord', verbose_name='address record'),
        ),
    ]
